
package fr.afpa.display;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import fr.afpa.beans.FilesGenerator;
import fr.control.Control;
import fr.model.SynthaxCommand;

public class Display {

	/**
	 * Affichage du terminal
	 * 
	 */
	public static void TerminalDisplay() {

		Scanner in = new Scanner(System.in);
		String command = "";

		while (command.contentEquals("Q") != true) {

			System.out.println("SQL:>");
			command = in.nextLine();

			if (command.contains("CREATE")) {

				SynthaxCommand.createTable(command);

			}

			if (command.contains("INSERT")) {

				SynthaxCommand.insertTo(command);

			}

			if (command.indexOf("SELECT*FROM") != -1) {

				SynthaxCommand.selectFrom(command);

			}

			else if (command.contains("SELECT")) {

				SynthaxCommand.selectFromColumn(command);

			}

			else if (command.equals("Q") || command.equals("q") || command.equals("quit") || command.equals("QUIT")) {

				CloseDisplay();
			}

		}
	}

//		System.out.println("+++++++++++++++++++++++++++++++++++++++");
//		System.out.println("+        GESTION DE VOS FICHIERS      +");
//		System.out.println("+                                     +");
//		System.out.println("+ 1 - Cr�er un nouveau fichier        +");
//		System.out.println("+ 2 - Lire un fichier existant        +");
//		System.out.println("+ 3 - Modifier un fichier             +");
//		System.out.println("+ 4 - Supprimer un fichier            +");
//		System.out.println("+ 5 - Quitter le programme            +");
//		System.out.println("+                                     +");
//		System.out.println("+++++++++++++++++++++++++++++++++++++++");

	/**
	 * Message affich� en quittant le programme
	 * 
	 */

	public static void CloseDisplay() {

		try {
			System.out.println(
					"Merci d'avoir choisi la soci�t� Julien-Ferdinand pour la gestion de vos fichiers personnels");
			TimeUnit.SECONDS.sleep(5);
			System.exit(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void ErrorDisplay() {

		/**
		 * Fait appel � la m�thode ReadFile de la classe FilesGenerator, qui renvoie un
		 * String. Avec un Regex, ce String est modifi� pour l'affichage, gr�ce � un
		 * StringBuffer
		 * 
		 */
	}

	public static void ArrayDisplay(String nomTable) {

		StringBuilder sb = new StringBuilder();
		int countLine = 0;
		StringBuilder SB = null;
		String str = FilesGenerator.ReadFile(nomTable);
		str = FilesGenerator.ReadFile(nomTable);

		int length = 0;
		str = str.replaceAll(";", " |");
		str = str.replaceAll(",", " |");
		for (String s : str.split(" ")) {
			length = 26 - s.length();

			StringBuilder sb2 = new StringBuilder();
			for (int i = 0; i < length; i++) {
				sb2.append(" ");
			}

			sb.append(s.toString() + sb2);
		}
		System.out.println(sb);
	}

}
