package fr.afpa.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import fr.control.Control;

public class FilesGenerator {

	static boolean BDDexists;

	/**
	 * Cette m�thode v�rifie que le dossier BDD existe, si pas, elle le cr�e. Puis,
	 * elle y cr�e le fichier avec le nom pass� en param�tre.
	 * 
	 */
	public static void CreateFile(String nomTable) {
		File f = new File("C://ENV//BDD");
		if (!f.exists()) {
			Path path = Paths.get("C://ENV//BDD");
			try {
				Files.createDirectory(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (!Control.ControlFileExist(nomTable)) {
			File file = new File("C://ENV//BDD//" + nomTable + ".cda");
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("le fichier a �t� cr�e");
		}
	}

	/**
	 * Cette m�thode ajoute l'ent�te � la premi�re ligne du fichier
	 * 
	 */
	public static void CreateTitle(String nomTable, String Title) {

		if (Control.ControlFileExist(nomTable)) {
			FileWriter myWriter;
			try {
				myWriter = new FileWriter("C://ENV//BDD//" + nomTable + ".cda");
				myWriter.write(Title.replaceAll(",", ";"));
				myWriter.close();
				System.out.println("l'ent�te a �t� cr�e");
			} catch (IOException e) {
				System.out.println("le fichier n'existe pas");
				e.printStackTrace();
			}

		}

	}

	/**
	 * methode de lecture et de renvoi de l'ent�te
	 */
	public static String ReadTitle(String nomTable) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}
		String s = null;
		try {
			s = in.readLine();
			in.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return s;
	}

	/**
	 * Cette m�thode va parcourir le fichier ligne par ligne avec le scanner, et
	 * concat�ner avec le StringBuilder. Le r�sultat du Stringbuilder est converti
	 * en String, et renvoy�.
	 */
	public static String ReadFile(String nomTable) {

		Scanner scanner;
		StringBuilder sb = new StringBuilder();
		String SB = null;
		try {
			scanner = new Scanner(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));

			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine());
				sb.append("\n");
				SB = sb.toString();

			}
		} catch (FileNotFoundException e) {
			System.out.println("le fichier n'existe pas");
			e.printStackTrace();
		}

		return SB;

	}

	/**
	 * Cette m�thode va ouvrir le fichier, et y �crire, en revenant � la ligne �
	 * chaque it�ration
	 * 
	 */
	public static void WriteFile(String nomTable, String line) {

		if (Control.ControlFileExist(nomTable)) {
			FileWriter myWriter;
			try {
				myWriter = new FileWriter("C://ENV//BDD//" + nomTable + ".cda", true);
				myWriter.write("\n" + line);
				myWriter.close();
			} catch (IOException e) {
				System.out.println("le fichier n'existe pas");
				e.printStackTrace();

			}
		}
	}

	/**
	 * m�thode comptant le nombre de lignes
	 */
	public static int CountLine(String nomTable) {

		Scanner scanner;
		StringBuilder sb = new StringBuilder();
		int countLine = 0;

		String str = FilesGenerator.ReadFile(nomTable);
		str = str.replaceAll(";", " |");
		str = str.replaceAll(",", " |");

		try {

			scanner = new Scanner(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));

			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine());
				countLine++;

			}
		} catch (FileNotFoundException e) {
			System.out.println("le fichier n'existe pas");
			e.printStackTrace();
		}

		return countLine;
	}

	public static String ReadFileNextLine(String nomTable) {

		Scanner scanner;
		StringBuilder sb = new StringBuilder();
		String SB = null;
		try {
			scanner = new Scanner(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));

			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine());
				sb.append("\n");
				SB = sb.toString();

			}
		} catch (FileNotFoundException e) {
			System.out.println("le fichier n'existe pas");
			e.printStackTrace();
		}

		return SB;

	}

	public static String ReadThisLine(String nomTable, int line) {

		Scanner scanner;
		FileReader file = null;

		try {
			file = new FileReader("C://ENV//BDD//" + nomTable + ".cda");
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		BufferedReader buffer = new BufferedReader(file);
		String thisLine = new String();

		try {
			scanner = new Scanner(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));

			for (int i = 0; i < 10; i++) {

				if (i == line)
					try {
						thisLine = buffer.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						buffer.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		} catch (FileNotFoundException e) {
			System.out.println("le fichier n'existe pas");
			e.printStackTrace();
		}

		return thisLine;

	}
}

//	public static void CreateTitle(String nomTable, String Title) throws FileNotFoundException {
//	// Verification que le fichier existe d�j�, avec la classe Control.
//	// if(Control.ControlFileExist) {
//
//	File file = new File("C://ENV//BDD//" + nomTable + ".cda");
//
//	if (file.length() == 0) {
//
//		try {
//			FileWriter myWriter = new FileWriter("C://ENV//BDD//" + nomTable + ".cda");
//
//			myWriter.write(Title);
//			myWriter.close();
//			System.out.println("all good");
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//
//		}
//	} else {
//		BufferedReader in = new BufferedReader(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));
//		String s;
//
//		try {
//			s = in.readLine();
//			in.close();
//			System.out.println(s);
//
//			try {
//				FileWriter myWriter = new FileWriter("C://ENV//BDD//" + nomTable + ".cda");
//				myWriter.write(s.replaceFirst(s, Title));					
//				myWriter.close();
//				System.out.println("all good");
//
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//
//			}
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	System.out.println("all good bis");
//
//}

//	public static StringBuilder [] MakeTab(String nomTable) {
//
//		Scanner scanner;
//		StringBuilder sb = new StringBuilder();
//		String SB = "";
//		int countLine = 0;
//		String firstLine = ReadTitle(nomTable);
//				
//		try {
//			scanner = new Scanner(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));
//
//			while (scanner.hasNextLine()) {
//				sb.append(scanner.nextLine());
//				countLine++;
//				
//			}
//		} catch (FileNotFoundException e) {
//			System.out.println("le fichier n'existe pas");
//			e.printStackTrace();
//		}
//
//		StringBuilder [] tabValues = new StringBuilder [countLine];
//		
//		try {
//			scanner = new Scanner(new FileReader("C://ENV//BDD//" + nomTable + ".cda"));
//
//				tabValues[0]= sb.append(scanner.nextLine());
//				
//				for (int i = 1; i < countLine; i++) {
//					tabValues[i] = sb.append(scanner.nextLine());
//				}
//				
//		} catch (FileNotFoundException e) {
//			System.out.println("le fichier n'existe pas");
//			e.printStackTrace();
//		}
//		System.out.println(tabValues[0]+" VALEUR "+tabValues[1]);
//		return tabValues;
//
//	}
