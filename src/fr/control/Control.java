package fr.control;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.display.Display;
import fr.model.SynthaxCommand;

public class Control {

	/**
	 * Methode de contr�le du login et du mot de passe utilisateur
	 */
	public static void ControlAuth() {

		Scanner in = new Scanner(System.in);
		System.out.println("Login");
		String login = in.nextLine();
		System.out.println("Password");
		String password = in.nextLine();

		if (login.equals(ReadPropertyFile.PropertyLoad("login"))
				&& password.equals(ReadPropertyFile.PropertyLoad("password"))) {

		}

		else {
			System.out.println("Identifiant incorrect");
			ControlAuth();
		}

	}

	/**
	 * Methode de controlant la taille des valeurs entr�es par l'utilisateur, devant
	 * �tre inf�rieure � 26 caract�res
	 */
	public static boolean ControlStringSize(String columnName) {

		if (columnName.length() <= 25) {

			return true;
		}
		return false;
	}

	/**
	 * methode v�rifiant l'existence du fichier
	 */
	public static boolean ControlFileExist(String nameFile) {

		File dir = new File("C:/ENV/BDD/" + nameFile + ".cda");
		boolean exists = dir.exists();

		return exists;
	}

	/**
	 * m�thode comparant le nombre de valeurs entr�es par l'utilsateur et le nombre
	 * colonnes du tableau
	 */
	public static boolean ControlValues(String firstLine, String values) {

		String[] fL = firstLine.split(";");
		String[] vL = values.split(",");

		if (vL.length - 1 != fL.length - 1) {

			return false;
		}
		return true;
	}
}
