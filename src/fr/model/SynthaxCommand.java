package fr.model;

import java.awt.List;
import java.io.*;
import java.util.ArrayList;
import fr.afpa.beans.FilesGenerator;
import fr.afpa.display.Display;
import fr.control.Control;
import java.lang.StringBuilder;

public class SynthaxCommand {

	static String[] instructions;
	static String[] nomTable;
	static String[] values;
	static String firstLine;

	/**
	 * methode CREATE TABLE : decoupage de la commande utilisateur afin de r�cup�rer
	 * les diff�rents arguments controle de la synthaxe en entr�e creation d'un
	 * fichier, nomm� � partir du "nomTable" sp�cifi� par l'utilisateur �criture de
	 * l'ent�te via l amethode CreateTitle()
	 */
	public static void createTable(String command) {

		instructions = command.split(" ");

		if (instructions[0].equals("CREATE") && instructions[1].equals("TABLE")) {

			nomTable = instructions[2].split("\\(");
			FilesGenerator.CreateFile(nomTable[0]);

			values = nomTable[1].split("\\)");
			FilesGenerator.CreateTitle(nomTable[0], values[0]);
			System.out.println("la table a �t� cr��");

		} else {
			System.out.println("commande inconnue");
		}
	}

	/**
	 * methode INSERT TO decoupage de la commande utilisateur afin de r�cup�rer les
	 * diff�rents arguments controle de la synthaxe en entr�e Lecture de fichier ou
	 * renvoi d'une erreur si fichier inexistant Ecriture des valeurs si la methode
	 * comparant le nombre de colonnes et de valeurs entr�es renvoie TRUE
	 */
	public static void insertTo(String command) {

		instructions = command.split(" ");

		if (instructions[0].equals("INSERT") && instructions[1].equals("INTO")) {

			firstLine = FilesGenerator.ReadTitle(instructions[2]);
			values = instructions[3].split("\\(");
			values[1] = values[1].replaceAll("[\\']", "");
			values[1] = values[1].replaceAll("[\\)]", "");

		}

		if (Control.ControlValues(firstLine, values[1]) == true) {
			FilesGenerator.WriteFile(instructions[2], values[1].replaceAll("[\\']", " "));
		} else {
			System.out.println("error");
		}
	}

	/**
	 * methode SELECT FROM decoupage de la commande utilisateur afin de r�cup�rer
	 * les diff�rents arguments controle de la synthaxe en entr�e appel de la
	 * m�thode ArrayDisplay();
	 */
	public static void selectFrom(String command) {

		instructions = command.split(" ");

		if (instructions[0].equals("SELECT*FROM")) {

			Display.ArrayDisplay(instructions[1]);
		}
	}

	public static void selectFromColumn(String command) {

		instructions = command.split(" ");
		firstLine = FilesGenerator.ReadTitle(instructions[3]);

		if (instructions[0].equals("SELECT") && instructions[2].equals("FROM")) {

			String[] tabFL = firstLine.split(";");
			int countLine = FilesGenerator.CountLine(instructions[3]);
			String[] tabLignes = new String[countLine];
			String[][] tab = new String[FilesGenerator.ReadFile(instructions[3]).split(",").length][countLine];
			int column = 0;

			for (int i = 0; i < tabFL.length; i++) {
				if (instructions[1].equals(tabFL[i])) {
					column = i;
				}
			}

			for (int i = 1; i < countLine; i++) {
				tabLignes = FilesGenerator.ReadThisLine(instructions[3], i).split(",");
				tab[i][column] = tabLignes[column];
			}

			System.out.println();
			System.out.println(tabFL[column]);
			System.out.println();

			for (int i = 1; i < countLine; i++) {
				System.out.println(tab[i][column]);
			}
			System.out.println();
		}
	}

	public void OrderBy() {

	}

	public void Update() {

	}

	public void UpdateSet() {

	}

	public void UpdateSetWhere() {

	}

}
